provider "azurerm" {
  version = "=2.0.0"
  features {}
}

resource "azurerm_resource_group" "test-rg" {
  name     = var.rg_name
  location = var.location
}

resource "azurerm_virtual_network" "test-vnet" {
  name                = var.vnet_name
  location            = azurerm_resource_group.test-rg.location
  resource_group_name = azurerm_resource_group.test-rg.name
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "test-subnet" {
  name                 = var.subnet_name
  resource_group_name  = azurerm_resource_group.test-rg.name
  virtual_network_name = azurerm_virtual_network.test-vnet.name
  address_prefix       = "10.0.1.0/24"
}

module "test_lb" {
  source = "../../"
  name = "test_lb"
  resource_group_name = var.rg_name
  location = var.location
  frontend = { 
    test_frontend = {
      subnet_id = azurerm_subnet.test-subnet.id
      }
  }
  backend_pool_names = ["test_backend"]
  azlb_probes = {
    test_probe = {
      port = 22
     }
  }
  azlb_rules = {
    test_lb_rule = {
      protocol =  "All"
      frontend_port = 0
      backend_port = 0
      probe_name = "test_probe"
      backendpool_name = "test_backend"
      frontend_name = "test_frontend"
    }
  }    
}
